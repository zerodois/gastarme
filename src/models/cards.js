const factory = ({ sequelize, Sequelize }) => {
  const tableName = 'cards';
  const config = { tableName };
  return sequelize.define('Card', {
    id: {
      type: Sequelize.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    walletId: {
      type: Sequelize.UUID,
      allowNull: false,
      references: {
        model: 'wallets',
        key: 'id',
      },
    },
    number: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: 'uniqueCardIndex',
    },
    cvv: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: 'uniqueCardIndex',
    },
    expiryYear: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: 'uniqueCardIndex',
    },
    expiryMonth: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: 'uniqueCardIndex',
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: 'uniqueCardIndex',
    },
    limit: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    payable: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    payment: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 31,
      },
    },
  }, config);
};

module.exports = factory;
