const factory = ({ sequelize, Sequelize, cardsModel: Card }) => {
  const tableName = 'wallets';
  const config = {
    tableName,
  };
  const Wallet = sequelize.define('Wallet', {
    id: {
      type: Sequelize.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    userId: {
      type: Sequelize.UUID,
      allowNull: false,
      unique: true,
      references: {
        model: 'users',
        key: 'id',
      },
    },
    limit: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    payable: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    cardInfo: {
      type: Sequelize.JSON,
      allowNull: false,
    },
  }, config);

  if (!Card) {
    return Wallet;
  }

  Wallet.hasMany(Card, {
    foreignKey: {
      name: 'walletId',
    },
  });
  Wallet.findWithCards = where => Wallet.findOne({
    where,
    include: [{ model: Card, as: 'Cards' }],
  });

  return Wallet;
};

module.exports = factory;
