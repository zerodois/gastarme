const factory = ({ sequelize, Sequelize }) => {
  const tableName = 'users';
  const config = { tableName };
  return sequelize.define('User', {
    id: {
      type: Sequelize.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    role: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },
  }, config);
};

module.exports = factory;
