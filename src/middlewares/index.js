const { validationResult } = require('express-validator/check');
const httpStatus = require('http-status');
const { get } = require('lodash');
const jwt = require('jsonwebtoken');
const { to } = require('../utils');

const walletFactory = ({ walletsModel }) => {
  const middleware = async (req, res, next) => {
    const [err, wallet] = await to(
      walletsModel.findOne({ where: { userId: req.user.id } }),
    );
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    if (!wallet) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: 'User needs to have a wallet' });
      return;
    }
    req.wallet = wallet;
    next();
  };

  return middleware;
};
exports.walletFactory = walletFactory;

/**
 * Parse JWT Token Middleware
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {Function} next
 */
const parseToken = (req, res, next) => {
  try {
    const auth = get(req.headers, 'authorization', '');
    if (!/^Bearer/.test(auth)) {
      next();
      return;
    }
    const token = auth.replace('Bearer ', '');
    const user = jwt.verify(token, process.env.JWT_SECRET);
    req.user = user;
    next();
  } catch (error) {
    next();
  }
};
exports.parseToken = parseToken;

/**
 * Middleware authentication
 */
const requireAuth = (req, res, next) => {
  if (!req.user) {
    res.status(httpStatus.UNAUTHORIZED).send();
    return;
  }
  next();
};
exports.requireAuth = requireAuth;

/**
 * Middleware authentication
 */
const requireAdmin = (req, res, next) => (
  requireAuth({ user: req.user.role }, res, next)
);
exports.requireAdmin = requireAdmin;

/**
 * Validation request
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {Function} next
 */
const validation = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    next();
    return;
  }
  res.status(httpStatus.UNPROCESSABLE_ENTITY).send(
    errors.array().map(item => ({
      [item.param]: item.msg,
    })),
  );
};
exports.validation = validation;
