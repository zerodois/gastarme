const httpStatus = require('http-status');
const Sequelize = require('sequelize-mock');
const middlewares = require('./index');
const walletsModelFactory = require('../models/wallets');
const { fakeContext } = require('../tests');
const { lilianaWallets } = require('../tests/fixtures/wallets');
const { sign } = require('../utils');

process.env.JWT_SECRET = 'MTG';
const sequelize = new Sequelize();
const walletsModel = walletsModelFactory({
  sequelize,
  Sequelize,
});

describe('Middleware tests', () => {
  afterEach(() => {
    walletsModel.$clearQueue();
  });
  it('Wallet middleware', async () => {
    walletsModel.$queueResult(walletsModel.build(lilianaWallets));
    const ctx = fakeContext({});
    const middleware = middlewares.walletFactory({ walletsModel });
    await middleware(ctx.req, ctx.res, () => null);
    expect(ctx.req.wallet).toMatchObject(lilianaWallets);
  });
  it('Wallet middleware user error', async () => {
    walletsModel.$queueResult(null);
    const ctx = fakeContext({});
    const middleware = middlewares.walletFactory({ walletsModel });
    await middleware(ctx.req, ctx.res, () => null);
    expect(ctx.response).toMatchObject({ error: 'User needs to have a wallet' });
    expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
  });
  it('Wallet middleware select error', async () => {
    const Model = {
      findOne: async () => {
        throw new Error('Connection error');
      },
    };
    const ctx = fakeContext({});
    const middleware = middlewares.walletFactory({ walletsModel: Model });
    await middleware(ctx.req, ctx.res, () => null);
    expect(ctx.response).toMatchObject({ error: 'Connection error' });
    expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
  });

  it('Parse Bearer Middleware', async () => {
    const next = jest.fn();
    const obj = { valid: true };
    const authorization = `Bearer ${sign(obj)}`;
    const req = { headers: { authorization } };
    middlewares.parseToken(req, null, next);
    expect(next.mock.calls.length).toEqual(1);
    expect(req.user).toMatchObject(obj);
  });
  it('Parse Bearer Middleware Without Token', async () => {
    const next = jest.fn();
    const req = { headers: {} };
    middlewares.parseToken(req, null, next);
    expect(next.mock.calls.length).toEqual(1);
  });
  it('Parse Bearer Middleware Error', async () => {
    const next = jest.fn();
    const authorization = 'Bearer Error';
    const req = { headers: { authorization } };
    middlewares.parseToken(req, null, next);
    expect(next.mock.calls.length).toEqual(1);
  });

  it('Require Auth Middleware', async () => {
    const next = jest.fn();
    const req = { user: true };
    middlewares.requireAuth(req, null, next);
    expect(next.mock.calls.length).toEqual(1);
  });
  it('Require Admin Middleware', async () => {
    const next = jest.fn();
    const req = { user: { role: 1 } };
    middlewares.requireAdmin(req, null, next);
    expect(next.mock.calls.length).toEqual(1);
  });
  it('Require Auth Middleware error', async () => {
    const req = {};
    const status = jest.fn();
    const res = {
      status: (statusCode) => {
        status(statusCode);
        return res;
      },
      send: jest.fn(),
    };
    middlewares.requireAuth(req, res, null);
    expect(status.mock.calls.length).toEqual(1);
    expect(status.mock.calls[0][0]).toEqual(httpStatus.UNAUTHORIZED);
    expect(res.send.mock.calls.length).toEqual(1);
  });
  it('Require admin Middleware error', async () => {
    const req = { user: { role: 0 } };
    const status = jest.fn();
    const res = {
      status: (statusCode) => {
        status(statusCode);
        return res;
      },
      send: jest.fn(),
    };
    middlewares.requireAdmin(req, res, null);
    expect(status.mock.calls.length).toEqual(1);
    expect(status.mock.calls[0][0]).toEqual(httpStatus.UNAUTHORIZED);
    expect(res.send.mock.calls.length).toEqual(1);
  });

  it('Validation Middleware', () => {
    const req = {};
    const next = jest.fn();
    middlewares.validation(req, null, next);
    expect(next.mock.calls.length).toEqual(1);
  });
  it('Validation Middleware error', () => {
    const req = {
      _validationErrors: [{
        location: 'body',
        param: 'email',
        value: undefined,
        msg: 'Invalid value',
      }],
    };
    const status = jest.fn();
    const res = {
      status: (statusCode) => {
        status(statusCode);
        return res;
      },
      send: jest.fn(),
    };
    middlewares.validation(req, res, null);
    expect(status.mock.calls.length).toEqual(1);
    expect(status.mock.calls[0][0]).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    expect(res.send.mock.calls.length).toEqual(1);
  });
});
