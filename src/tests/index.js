const fakeContext = (data) => {
  const context = { statusCode: 200 };
  context.res = {
    send: response => Object.assign(context, { response }),
    status: (status) => {
      context.statusCode = status;
      return context.res;
    },
  };
  context.response = null;
  const user = {
    id: 'LILI-VESS',
  };
  const params = {
    id: 'AAAA-ZZZZ',
  };
  const wallet = {
    id: 'AAAA-ZZZZ',
  };
  context.req = {
    user,
    wallet,
    params,
    ...data,
  };
  return context;
};
exports.fakeContext = fakeContext;
