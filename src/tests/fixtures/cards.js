module.exports = [
  {
    id: 'AAAA-BBBB',
    walletId: 'AAAA-ZZZZ',
    number: '5233 9546 5626 5275',
    cvv: 416,
    expiryYear: 2020,
    expiryMonth: 5,
    limit: 1900,
    payable: 0,
    payment: 15,
  },
  {
    id: 'AAAA-CCCC',
    walletId: 'AAAA-ZZZZ',
    number: '5400 9833 2383 0382',
    cvv: 221,
    expiryYear: 2020,
    expiryMonth: 9,
    limit: 2100,
    payable: 100,
    payment: 29,
  },
];
