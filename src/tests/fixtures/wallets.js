const lilianaWallets = {
  id: 'AAAA-ZZZZ',
  userId: 'LILI-VESS',
  limit: 0,
  payable: 0,
  cardInfo: {
    number: '4111 1111 1111 1111',
    cvv: 737,
    expiryYear: 2020,
    expiryMonth: 10,
    name: 'Liliana V.',
  },
};
const jaceWallets = {
  id: 'AAAA-YYYY',
  userId: 'JACE-BELE',
  limit: 0,
  payable: 0,
  cardInfo: {
    number: '4111 1111 1111 1112',
    cvv: 737,
    expiryYear: 2020,
    expiryMonth: 10,
    name: 'Jace Beleren',
  },
};

module.exports = {
  lilianaWallets,
  jaceWallets,
};
