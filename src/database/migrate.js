require('dotenv').config();
const database = require('./index');

const User = require('../models/users');
const Wallet = require('../models/wallets');
const Card = require('../models/cards');

const tables = {
  User,
  Wallet,
  Card,
};

const options = {};

const Sync = ([table, ...rest]) => (
  table(database).sync(options)
    .then(() => {
      if (!rest.length) {
        return Promise.resolve(true);
      }
      return Sync(rest);
    })
);

(async () => {
  await database.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
  await Sync(Object.values(tables));
})()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(0);
  });
