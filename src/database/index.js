const Sequelize = require('sequelize');
const config = require('../config');

const env = process.env.NODE_ENV;
module.exports = Object.freeze({
  Sequelize,
  // @ts-ignore
  sequelize: new Sequelize(config[env]),
});
