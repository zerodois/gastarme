const SequelizeMock = require('sequelize-mock');
const httpStatus = require('http-status');
const { liliana } = require('../tests/fixtures/users');
const { fakeContext } = require('../tests');
const userModelFactory = require('../models/users');
const userControllerFactory = require('./users');

const hash = () => 'VERYSTRONGHASH';
const mock = new SequelizeMock();

describe('User Controller Tests', () => {
  describe('Create Tests', () => {
    const usersModel = userModelFactory({ sequelize: mock, Sequelize: SequelizeMock });
    it('Success create', async () => {
      const controller = userControllerFactory({ hash, usersModel });
      const ctx = fakeContext({ body: liliana });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({
        ...liliana,
        password: 'VERYSTRONGHASH',
      });
    });

    it('Create admin error', async () => {
      const controller = userControllerFactory({ hash, usersModel });
      const ctx = fakeContext({ body: { ...liliana, role: 1 } });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Only admin can register other admins' });
      expect(ctx.statusCode).toEqual(httpStatus.UNAUTHORIZED);
    });

    it('Create error', async () => {
      const Model = {
        build: (data) => {
          const user = usersModel.build(data);
          user.$addValidationError('email', 'Not a valid email address', 'InvalidEmail');
          return user;
        },
      };
      const controller = userControllerFactory({ hash, usersModel: Model });
      const ctx = fakeContext({ body: {} });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Validation Error' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });

  describe('User login', () => {
    const usersModel = userModelFactory({ sequelize: mock, Sequelize: SequelizeMock });
    it('Success login', async () => {
      const verify = () => true;
      const sign = () => 'TOKENMANEIRO';
      const controller = userControllerFactory({
        hash,
        usersModel,
        verify,
        sign,
      });
      const ctx = fakeContext({ body: liliana });
      await controller.login(ctx.req, ctx.res);
      const canon = {
        token: 'TOKENMANEIRO',
      };
      expect(ctx.response).toMatchObject(canon);
    });

    it('Invalid Credentials', async () => {
      const verify = () => false;
      const controller = userControllerFactory({ hash, usersModel, verify });
      const ctx = fakeContext({ body: liliana });
      await controller.login(ctx.req, ctx.res);
      const canon = {
        message: 'Invalid credentials.',
      };
      expect(ctx.response).toMatchObject(canon);
      expect(ctx.statusCode).toEqual(httpStatus.UNAUTHORIZED);
    });
  });
});
