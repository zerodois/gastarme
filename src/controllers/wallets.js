const httpStatus = require('http-status');
const { get } = require('lodash');
const { to } = require('../utils');

const factory = ({ walletsModel: Wallet }) => {
  const all = async (req, res) => {
    const limit = get(req.query, 'maxResults', 50);
    const page = get(req.query, 'page', 1);
    const offset = (page - 1) * limit;
    const [err, wallets] = await to(Wallet.findAll({
      limit,
      offset,
    }));
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    res.send(wallets);
  };

  return Object.freeze({
    all,
  });
};

module.exports = factory;
