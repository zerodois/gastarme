const httpStatus = require('http-status');
const { get } = require('lodash');
const { ResponseError } = require('../utils');
const { organize } = require('../utils/cards');

const factory = ({ walletsModel: Wallet, sequelize, today }) => {
  const buy = async (req, res) => {
    try {
      let { amount } = req.body;
      const wallet = await Wallet.findWithCards({ userId: req.user.id });
      if (!wallet) {
        throw new ResponseError({ message: 'Wallet not found', statusCode: httpStatus.NOT_FOUND });
      }
      const avaible = Math.max(0, wallet.limit - wallet.payable);
      if (amount > avaible) {
        throw new ResponseError('Amount must be <= wallet limit');
      }
      const day = today || new Date().getDate();
      const cards = organize({ amount, cards: wallet.Cards, today: day });
      sequelize.transaction((t) => {
        const promises = cards.map((card, index) => {
          const money = card.limit - card.payable;
          const cost = Math.min(money, amount);
          cards[index].payable += cost;
          amount -= cost;
          return card.save({ transaction: t });
        });
        wallet.payable += req.body.amount;
        promises.push(wallet.save({ transaction: t }));
        return Promise.all(promises);
      });

      res.send({ success: true });
    } catch (error) {
      res
        .status(get(error, 'statusCode', httpStatus.UNPROCESSABLE_ENTITY))
        .send({ error: error.message });
    }
  };

  return Object.freeze({
    buy,
  });
};
module.exports = factory;
