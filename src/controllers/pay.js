const httpStatus = require('http-status');
const { get } = require('lodash');
const { ResponseError } = require('../utils');
const { organize } = require('../utils/cards');

const factory = ({ walletsModel: Wallet, sequelize, today = null }) => {
  const pay = async (req, res) => {
    try {
      let { amount } = req.body;
      const wallet = await Wallet.findWithCards({ userId: req.user.id });
      if (!wallet) {
        throw new ResponseError({ message: 'Wallet not found', statusCode: httpStatus.NOT_FOUND });
      }
      amount = Math.min(amount, wallet.payable);
      const day = today || new Date().getDate();
      const cards = organize({
        amount,
        cards: wallet.Cards,
        today: day,
        payment: true,
      });
      sequelize.transaction((t) => {
        const promises = cards.map((card, index) => {
          const toPay = Math.min(amount, card.payable);
          cards[index].payable -= toPay;
          amount -= toPay;
          return card.save({ transaction: t });
        });
        wallet.payable -= Math.min(wallet.payable, req.body.amount);
        promises.push(wallet.save({ transaction: t }));
        return Promise.all(promises);
      });

      res.send({ wallet });
    } catch (error) {
      res
        .status(get(error, 'statusCode', httpStatus.UNPROCESSABLE_ENTITY))
        .send({ error: error.message });
    }
  };

  return Object.freeze({
    pay,
  });
};
module.exports = factory;
