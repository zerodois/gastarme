const SequelizeMock = require('sequelize-mock');
const httpStatus = require('http-status');
const { lilianaWallets, jaceWallets } = require('../tests/fixtures/wallets');
const { fakeContext } = require('../tests');
const walletsModelFactory = require('../models/wallets');
const cardsModelFactory = require('../models/cards');
const walletsControllerFactory = require('./wallets');

const mock = new SequelizeMock();
const Card = cardsModelFactory({ Sequelize: SequelizeMock, sequelize: mock });

describe('Wallet Controller Tests', () => {
  describe('Get all wallets', () => {
    const walletsModel = walletsModelFactory({
      sequelize: mock,
      Sequelize: SequelizeMock,
      cardsModel: Card,
    });
    afterEach(() => {
      walletsModel.$clearQueue();
    });
    it('Success', async () => {
      walletsModel.$queueResult([
        walletsModel.build(lilianaWallets),
        walletsModel.build(jaceWallets),
      ]);
      const controller = walletsControllerFactory({ walletsModel });
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.all(ctx.req, ctx.res);
      expect(ctx.response[0]).toMatchObject({
        ...lilianaWallets,
      });
      expect(ctx.response[1]).toMatchObject({
        ...jaceWallets,
      });
      walletsModel.$clearQueue();
    });
    it('find Error', async () => {
      const Model = {
        findAll: async () => {
          throw new Error('Connection error');
        },
      };
      const controller = walletsControllerFactory({ walletsModel: Model });
      const ctx = fakeContext({});
      await controller.all(ctx.req, ctx.res);
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
      expect(ctx.response).toMatchObject({ error: 'Connection error' });
    });
  });
});
