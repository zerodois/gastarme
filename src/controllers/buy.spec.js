const SequelizeMock = require('sequelize-mock');
const httpStatus = require('http-status');
const lilianaCards = require('../tests/fixtures/cards');
const { lilianaWallets } = require('../tests/fixtures/wallets');
const { fakeContext } = require('../tests');
const cardsModelFactory = require('../models/cards');
const walletModelFactory = require('../models/wallets');
const buyControllerFactory = require('./buy');

const mock = new SequelizeMock();

describe('Buy Controller Tests', () => {
  const cardsModel = cardsModelFactory({
    sequelize: mock,
    Sequelize: SequelizeMock,
  });
  const walletsModel = walletModelFactory({
    sequelize: mock,
    Sequelize: SequelizeMock,
    cardsModel,
  });
  afterEach(() => {
    walletsModel.$clearQueue();
    cardsModel.$clearQueue();
  });

  it('Wallet withou limit', async () => {
    walletsModel.$queueResult(walletsModel.build(lilianaCards[0]));
    const controller = buyControllerFactory({ walletsModel, sequelize: mock });
    const ctx = fakeContext({ body: { amount: 7000 } });
    await controller.buy(ctx.req, ctx.res);
    expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    expect(ctx.response).toMatchObject({
      error: 'Amount must be <= wallet limit',
    });
  });

  it('Wallet not found', async () => {
    const Model = {
      findWithCards: () => null,
    };
    const controller = buyControllerFactory({ walletsModel: Model, sequelize: mock });
    const ctx = fakeContext({ body: { amount: 7000 } });
    await controller.buy(ctx.req, ctx.res);
    expect(ctx.statusCode).toEqual(httpStatus.NOT_FOUND);
    expect(ctx.response).toMatchObject({
      error: 'Wallet not found',
    });
  });

  it('Buy with 1 card', async () => {
    const Model = {
      findWithCards: async () => {
        const wallet = walletsModel.build({ ...lilianaWallets, limit: 2000 });
        wallet.Cards = lilianaCards.map(card => cardsModel.build(card));
        return wallet;
      },
    };
    const controller = buyControllerFactory({
      sequelize: mock,
      walletsModel: Model,
    });
    const ctx = fakeContext({ body: { amount: 400 } });
    await controller.buy(ctx.req, ctx.res);
    expect(ctx.response).toMatchObject({ success: true });
    expect(ctx.statusCode).toEqual(httpStatus.OK);
  });
});
