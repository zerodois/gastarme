const httpStatus = require('http-status');
const { get } = require('lodash');
const { to } = require('../utils');

const factory = (container) => {
  const {
    usersModel: User,
    hash,
    verify,
    sign,
  } = container;
  const create = async (req, res) => {
    const password = await hash(req.body.password);
    if (get(req.body, 'role', 0) && get(req, 'user.role', 0) < 1) {
      res
        .status(httpStatus.UNAUTHORIZED)
        .send({ error: 'Only admin can register other admins' });
      return;
    }
    const user = User.build({ ...req.body, password });
    const [err] = await to(user.save());
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    res.send(user);
  };

  const login = async (req, res) => {
    const where = { email: req.body.email };
    const user = await User.findOne({ where });
    const correct = await verify(req.body.password, get(user, 'password', ''));
    if (!user || !correct) {
      res.status(401).send({ message: 'Invalid credentials.' });
      return;
    }
    res.send({ token: sign(user.toJSON()) });
  };

  return Object.freeze({
    create,
    login,
  });
};

module.exports = factory;
