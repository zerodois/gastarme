const SequelizeMock = require('sequelize-mock');
const httpStatus = require('http-status');
const lilianaCards = require('../tests/fixtures/cards');
const { lilianaWallets } = require('../tests/fixtures/wallets');
const { fakeContext } = require('../tests');
const cardsModelFactory = require('../models/cards');
const walletsModelFactory = require('../models/wallets');
const cardsControllerFactory = require('./cards');

const mock = new SequelizeMock();
const walletsModel = walletsModelFactory({
  sequelize: mock,
  Sequelize: SequelizeMock,
});
const wallet = walletsModel.build(lilianaWallets);
const Wallet = {
  findOne: () => Promise.resolve(wallet),
};

describe('Cards Controller Tests', () => {
  const cardsModel = cardsModelFactory({
    sequelize: mock,
    Sequelize: SequelizeMock,
  });
  afterEach(() => {
    cardsModel.$clearQueue();
  });
  describe('Create Tests', () => {
    it('Success create', async () => {
      cardsModel.$queueResult(null);
      const controller = cardsControllerFactory({ cardsModel, walletsModel: Wallet });
      const toCreate = { ...lilianaCards[0] };
      delete toCreate.id;
      const ctx = fakeContext({ body: toCreate });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({
        ...toCreate,
      });
      expect(ctx.response.createdAt).not.toBeNull();
      expect(ctx.response.updatedAt).not.toBeNull();
    });

    it('Error on Insert', async () => {
      const Model = {
        findOne: () => null,
        build: (data) => {
          const card = cardsModel.build(data);
          card.$addValidationError('userId', 'Not a valid walletId', 'InvalidWalletId');
          return card;
        },
      };
      const controller = cardsControllerFactory({ cardsModel: Model, walletsModel: Wallet });
      const ctx = fakeContext({ body: {} });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Validation Error' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    });

    it('Error card exists', async () => {
      const controller = cardsControllerFactory({ cardsModel });
      const ctx = fakeContext({ body: lilianaCards[0] });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Credit Card exists' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });

  describe('Get user cards', () => {
    it('Sucess', async () => {
      cardsModel.$queueResult(
        lilianaCards.map(card => cardsModel.build(card)),
      );
      const controller = cardsControllerFactory({ cardsModel });
      const ctx = fakeContext({});
      await controller.getByUser(ctx.req, ctx.res);
      lilianaCards.forEach((card, index) => {
        expect(ctx.response[index]).toMatchObject({ ...card });
      });
    });

    it('Select error', async () => {
      const Model = {
        findAll: async () => {
          throw new Error('Connection error');
        },
      };
      const controller = cardsControllerFactory({ cardsModel: Model });
      const ctx = fakeContext({});
      await controller.getByUser(ctx.req, ctx.res);
      expect(ctx.statusCode).toEqual(httpStatus.INTERNAL_SERVER_ERROR);
      expect(ctx.response).toMatchObject({
        error: 'Connection error',
      });
    });
  });

  describe('Delete card', () => {
    it('Delete success', async () => {
      cardsModel.$queueResult(1);
      const controller = cardsControllerFactory({ cardsModel });
      const ctx = fakeContext({});
      ctx.req.params.id = lilianaCards[0].id;
      await controller.drop(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ success: true });
    });
    it('Card not exists', async () => {
      cardsModel.$queueResult(0);
      const controller = cardsControllerFactory({ cardsModel });
      const ctx = fakeContext({ body: {} });
      ctx.req.params.id = lilianaCards[0].id;
      await controller.drop(ctx.req, ctx.res);
      expect(ctx.statusCode).toEqual(httpStatus.NOT_FOUND);
      expect(ctx.response).toMatchObject({
        error: 'Card not exists',
      });
    });
    it('Drop error', async () => {
      cardsModel.$queueFailure('Operation error');
      const controller = cardsControllerFactory({ cardsModel });
      const ctx = fakeContext({ body: {} });
      ctx.req.params.id = lilianaCards[0].id;
      await controller.drop(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Operation error' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });
});
