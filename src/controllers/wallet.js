const httpStatus = require('http-status');
const { get } = require('lodash');
const random = require('random-world');
const { to, ResponseError } = require('../utils');

const factory = ({ walletsModel: Wallet, cardsModel: Card = null }) => {
  const create = async (req, res) => {
    const expiryYear = new Date().getFullYear() + 5;
    const expiryMonth = new Date().getMonth();
    const number = random
      .ccnumber()
      .replace(/(\d{4})(\d{4})(\d{4})(\d+)/, '$1 $2 $3 $4');
    const cardInfo = {
      number,
      expiryMonth,
      expiryYear,
      cvv: Number(random.cvv()),
      name: req.user.name,
    };
    const toCreate = {
      limit: 0,
      payable: 0,
      cardInfo,
      userId: req.user.id,
    };
    const wallet = Wallet.build(toCreate);
    const [err] = await to(wallet.save());
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    res.send(wallet);
  };

  const update = async (req, res) => {
    try {
      const { wallet } = req;
      const where = { walletId: wallet.userId };
      const cards = await Card.findAll({ where });
      const limitSum = cards.reduce((prev, card) => prev + card.limit, 0);
      if (req.body.limit > limitSum) {
        throw new ResponseError('Wallet limit must to be <= credit cards limit sum');
      }
      await wallet.update(req.body);
      res.send(wallet);
    } catch (error) {
      res
        .status(get(error, 'statusCode', httpStatus.UNPROCESSABLE_ENTITY))
        .send({ error: error.message });
    }
  };

  const getByUser = async (req, res) => {
    const where = { userId: req.user.id };
    const [err, wallet] = await to(Wallet.findOne({ where }));
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    if (!wallet) {
      res.status(httpStatus.NOT_FOUND).send({ error: 'User without wallet' });
      return;
    }
    res.send(wallet);
  };

  const drop = async (req, res) => {
    const where = { id: req.params.id };
    const [err, affecteds] = await to(Wallet.destroy({ where }));
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    if (!affecteds) {
      res.status(httpStatus.NOT_FOUND).send({ error: 'Wallet not exists' });
      return;
    }
    res.send({ success: true });
  };

  return Object.freeze({
    create,
    drop,
    update,
    getByUser,
  });
};

module.exports = factory;
