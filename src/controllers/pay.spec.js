const SequelizeMock = require('sequelize-mock');
const httpStatus = require('http-status');
const lilianaCards = require('../tests/fixtures/cards');
const { lilianaWallets } = require('../tests/fixtures/wallets');
const { fakeContext } = require('../tests');
const cardsModelFactory = require('../models/cards');
const walletModelFactory = require('../models/wallets');
const payControllerFactory = require('./pay');

const mock = new SequelizeMock();

describe('Pay Controller Tests', () => {
  const cardsModel = cardsModelFactory({
    sequelize: mock,
    Sequelize: SequelizeMock,
  });
  const walletsModel = walletModelFactory({
    sequelize: mock,
    Sequelize: SequelizeMock,
    cardsModel,
  });
  afterEach(() => {
    walletsModel.$clearQueue();
    cardsModel.$clearQueue();
  });

  it('Wallet not found', async () => {
    const Model = {
      findWithCards: () => null,
    };
    const controller = payControllerFactory({ walletsModel: Model, sequelize: mock });
    const ctx = fakeContext({ body: { amount: 7000 } });
    await controller.pay(ctx.req, ctx.res);
    expect(ctx.statusCode).toEqual(httpStatus.NOT_FOUND);
    expect(ctx.response).toMatchObject({
      error: 'Wallet not found',
    });
  });

  it('Pay 1 card', async () => {
    const wallet = walletsModel.build({ ...lilianaWallets, payable: 300 });
    const cards = [...lilianaCards];
    cards[0] = { ...cards[0], payable: 100 };
    cards[1] = { ...cards[1], payable: 200 };
    wallet.Cards = cards.map(card => cardsModel.build(card));
    const Model = {
      findWithCards: async () => wallet,
    };
    const controller = payControllerFactory({
      sequelize: mock,
      walletsModel: Model,
      today: 28,
    });
    const ctx = fakeContext({ body: { amount: 150 } });
    await controller.pay(ctx.req, ctx.res);
    expect(ctx.response).toMatchObject({ wallet: { payable: 150 } });
    const card1 = ctx.response.wallet.Cards.find(card => card.id === cards[0].id);
    const card2 = ctx.response.wallet.Cards.find(card => card.id === cards[1].id);
    expect(card1).toMatchObject({ payable: 0 });
    expect(card2).toMatchObject({ payable: 150 });
    expect(ctx.statusCode).toEqual(httpStatus.OK);
  });

  it('Pay all', async () => {
    const wallet = walletsModel.build({ ...lilianaWallets, payable: 300 });
    const cards = [...lilianaCards];
    cards[0] = { ...cards[0], payable: 200 };
    cards[1] = { ...cards[1], payable: 100 };
    wallet.Cards = cards.map(card => cardsModel.build(card));
    const Model = {
      findWithCards: async () => wallet,
    };
    const controller = payControllerFactory({
      sequelize: mock,
      walletsModel: Model,
    });
    const ctx = fakeContext({ body: { amount: 400 } });
    await controller.pay(ctx.req, ctx.res);
    expect(ctx.response).toMatchObject({
      wallet: {
        payable: 0,
        Cards: [
          { payable: 0 },
          { payable: 0 },
        ],
      },
    });
    expect(ctx.statusCode).toEqual(httpStatus.OK);
  });
});
