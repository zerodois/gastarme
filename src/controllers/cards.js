const httpStatus = require('http-status');
const { get } = require('lodash');
const { to, ResponseError } = require('../utils');

const factory = ({ cardsModel: Card }) => {
  const create = async (req, res) => {
    try {
      const cardBody = req.body;
      const where = {
        number: cardBody.number,
        cvv: cardBody.cvv,
        expiryYear: cardBody.expiryYear,
        expiryMonth: cardBody.expiryMonth,
        name: cardBody.name,
      };
      const existent = await Card.findOne({ where });
      if (existent) {
        throw new ResponseError('Credit Card exists');
      }
      const card = Card.build({ ...cardBody, walletId: req.wallet.id });
      const [err] = await to(card.save());
      if (err) {
        res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
        return;
      }
      res.send(card);
    } catch (error) {
      res
        .status(get(error, 'statusCode', httpStatus.UNPROCESSABLE_ENTITY))
        .send({ error: error.message });
    }
  };

  const getByUser = async (req, res) => {
    const where = { walletId: req.wallet.id };
    const [err, cards] = await to(Card.findAll({ where }));
    if (err) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ error: err.message });
      return;
    }
    res.send(cards);
  };

  const drop = async (req, res) => {
    const where = {
      id: req.params.id,
    };
    const [err, affecteds] = await to(Card.destroy({ where }));
    if (err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send({ error: err.message });
      return;
    }
    if (!affecteds) {
      res.status(httpStatus.NOT_FOUND).send({ error: 'Card not exists' });
      return;
    }
    res.send({ success: true });
  };

  return Object.freeze({
    create,
    drop,
    getByUser,
  });
};

module.exports = factory;
