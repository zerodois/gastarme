const SequelizeMock = require('sequelize-mock');
const httpStatus = require('http-status');
const { lilianaWallets } = require('../tests/fixtures/wallets');
const { fakeContext } = require('../tests');
const walletsModelFactory = require('../models/wallets');
const cardsModelFactory = require('../models/cards');
const walletControllerFactory = require('./wallet');

const mock = new SequelizeMock();
const Card = cardsModelFactory({ Sequelize: SequelizeMock, sequelize: mock });

describe('Wallet Controller Tests', () => {
  describe('Create Tests', () => {
    const walletsModel = walletsModelFactory({
      sequelize: mock,
      Sequelize: SequelizeMock,
      cardsModel: Card,
    });

    it('Success create', async () => {
      const controller = walletControllerFactory({ walletsModel });
      const toCreate = { ...lilianaWallets };
      delete toCreate.id;
      delete toCreate.cardInfo;
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({
        ...toCreate,
      });
      expect(ctx.response.cardInfo).not.toBeNull();
      expect(ctx.response.createdAt).not.toBeNull();
      expect(ctx.response.updatedAt).not.toBeNull();
    });

    it('Create error on Insert', async () => {
      const Model = {
        build: (data) => {
          const wallet = walletsModel.build(data);
          wallet.$addValidationError('userId', 'Not a valid userId', 'InvalidUserId');
          return wallet;
        },
      };
      const controller = walletControllerFactory({ walletsModel: Model });
      const ctx = fakeContext({ body: {} });
      await controller.create(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Validation Error' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });

  describe('Wallet update tests', () => {
    const walletsModel = walletsModelFactory({
      sequelize: mock,
      Sequelize: SequelizeMock,
      cardsModel: Card,
    });
    const cardsModel = cardsModelFactory({
      sequelize: mock,
      Sequelize: SequelizeMock,
    });

    it('Success update', async () => {
      const wallet = walletsModel.build(lilianaWallets);
      cardsModel.$queueResult([
        cardsModel.build({ limit: 10, walletId: lilianaWallets.id }),
        cardsModel.build({ limit: 2000, walletId: lilianaWallets.id }),
      ]);
      const controller = walletControllerFactory({ walletsModel, cardsModel });
      const toUpdate = { ...lilianaWallets, limit: 900 };
      const ctx = fakeContext({ body: toUpdate });
      ctx.req.wallet = wallet;
      await controller.update(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({
        ...toUpdate,
      });
      expect(ctx.statusCode).toEqual(httpStatus.OK);
      walletsModel.$clearQueue();
      cardsModel.$clearQueue();
    });

    it('Update error on command', async () => {
      const wallet = walletsModel.build(lilianaWallets);
      wallet.$addValidationError('userId', 'Not a valid userId', 'InvalidUserId');
      walletsModel.$queueResult(wallet);
      const controller = walletControllerFactory({ walletsModel, cardsModel });
      const ctx = fakeContext({ body: {} });
      ctx.req.wallet = wallet;
      await controller.update(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Validation Error' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
      walletsModel.$clearQueue();
    });

    it('Update wallet with limit greather than credit card limit sum!', async () => {
      const wallet = walletsModel.build(lilianaWallets);
      cardsModel.$queueResult([
        cardsModel.build({ limit: 10, walletId: lilianaWallets.id }),
        cardsModel.build({ limit: 2000, walletId: lilianaWallets.id }),
      ]);
      const controller = walletControllerFactory({ walletsModel, cardsModel });
      const toUpdate = { ...lilianaWallets, limit: 5000 };
      const ctx = fakeContext({ body: toUpdate });
      ctx.req.wallet = wallet;
      await controller.update(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({
        error: 'Wallet limit must to be <= credit cards limit sum',
      });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
      walletsModel.$clearQueue();
      cardsModel.$clearQueue();
    });
  });

  describe('Get wallet info', () => {
    const walletsModel = walletsModelFactory({
      sequelize: mock,
      Sequelize: SequelizeMock,
      cardsModel: Card,
    });
    afterEach(() => {
      walletsModel.$clearQueue();
    });
    it('Wallet exists', async () => {
      walletsModel.$queueResult(walletsModel.build(lilianaWallets));
      const controller = walletControllerFactory({ walletsModel });
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.getByUser(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({
        ...lilianaWallets,
      });
      walletsModel.$clearQueue();
    });
    it('find Error', async () => {
      const Model = {
        findOne: async () => {
          throw new Error('Connection error');
        },
      };
      const controller = walletControllerFactory({ walletsModel: Model });
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.getByUser(ctx.req, ctx.res);
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
      expect(ctx.response).toMatchObject({ error: 'Connection error' });
    });
    it('User without Wallet', async () => {
      walletsModel.$queueResult(null);
      const controller = walletControllerFactory({ walletsModel });
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.getByUser(ctx.req, ctx.res);
      expect(ctx.statusCode).toEqual(httpStatus.NOT_FOUND);
      expect(ctx.response).toMatchObject({
        error: 'User without wallet',
      });
    });
  });

  describe('Delete wallet', () => {
    const walletsModel = walletsModelFactory({
      sequelize: mock,
      Sequelize: SequelizeMock,
      cardsModel: Card,
    });
    afterEach(() => {
      walletsModel.$clearQueue();
    });
    it('Delete success', async () => {
      walletsModel.$queueResult(1);
      const controller = walletControllerFactory({ walletsModel });
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.drop(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ success: true });
    });
    it('Wallet not exists', async () => {
      walletsModel.$queueResult(0);
      const controller = walletControllerFactory({ walletsModel });
      const ctx = fakeContext({ body: lilianaWallets });
      await controller.drop(ctx.req, ctx.res);
      expect(ctx.statusCode).toEqual(httpStatus.NOT_FOUND);
      expect(ctx.response).toMatchObject({
        error: 'Wallet not exists',
      });
    });
    it('Drop error', async () => {
      walletsModel.$queueFailure('Operation error');
      const controller = walletControllerFactory({ walletsModel });
      const ctx = fakeContext({ body: {} });
      await controller.drop(ctx.req, ctx.res);
      expect(ctx.response).toMatchObject({ error: 'Operation error' });
      expect(ctx.statusCode).toEqual(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });
});
