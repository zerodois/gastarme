const { updateSchema } = require('../schemas/wallet');
const { requireAuth, requireAdmin, validation } = require('../middlewares');

const factory = ({ router, walletController, walletMiddleware }) => {
  router.post('/', requireAuth, walletController.create);
  router.put('/', requireAuth, walletMiddleware, updateSchema, validation, walletController.update);
  router.get('/', requireAuth, walletController.getByUser);
  router.delete('/:id', requireAuth, requireAdmin, walletController.drop);
  return router;
};

module.exports = factory;
