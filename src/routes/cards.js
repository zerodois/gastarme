const { requireAuth, validation } = require('../middlewares');
const { createSchema } = require('../schemas/cards');

const factory = ({ router, cardsController, walletMiddleware }) => {
  router.get('/', requireAuth, walletMiddleware, cardsController.getByUser);
  router.post('/', requireAuth, walletMiddleware, createSchema, validation, cardsController.create);
  router.delete('/:id', requireAuth, cardsController.drop);
  return router;
};

module.exports = factory;
