const { schema } = require('../schemas/buyAndPay');
const { requireAuth, validation } = require('../middlewares');

const factory = ({ router, payController }) => {
  router.post('/', requireAuth, schema, validation, payController.pay);
  return router;
};

module.exports = factory;
