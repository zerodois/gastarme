const { loginSchema, createSchema } = require('../schemas/users');
const { validation } = require('../middlewares');

/**
 * User routes factory
 * @param {object} container
 */
const factory = ({ router, usersController }) => {
  router.post('/', createSchema, validation, usersController.create);
  router.post('/login', loginSchema, validation, usersController.login);
  return router;
};

module.exports = factory;
