const { schema } = require('../schemas/buyAndPay');
const { requireAuth, validation } = require('../middlewares');

const factory = ({ router, buyController }) => {
  router.post('/', requireAuth, schema, validation, buyController.buy);

  return router;
};

module.exports = factory;
