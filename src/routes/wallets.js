const { requireAuth, requireAdmin } = require('../middlewares');

const factory = ({ router, walletsController }) => {
  router.get('/', requireAuth, requireAdmin, walletsController.all);
  return router;
};

module.exports = factory;
