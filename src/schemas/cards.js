const { checkSchema } = require('express-validator/check');

const createSchema = checkSchema({
  number: {
    matches: {
      options: /^\d{4}\s?\d{4}\s?\d{4}\s?\d{4,7}$/,
      errorMessage: 'Credit card must be XXXX XXXX XXXX XXXX',
    },
  },
  name: {
    isEmpty: {
      negated: true,
      errorMessage: 'Name is required',
    },
  },
  cvv: {
    isInt: {
      options: {
        min: 0,
        max: 999,
      },
      errorMessage: 'CVV must be >= 000  and <= 999',
    },
  },
  expiryYear: {
    isInt: {
      options: {
        min: 2019,
        max: 2999,
      },
      errorMessage: 'expiryYear must be >= 2019  and <= 2999',
    },
  },
  expiryMonth: {
    isInt: {
      options: {
        min: 1,
        max: 12,
      },
      errorMessage: 'expiryMonth must be >= 1  and <= 12',
    },
  },
  limit: {
    isInt: {
      options: {
        min: 0,
      },
      errorMessage: 'limit must be >= 0',
    },
  },
  payment: {
    isInt: {
      options: {
        min: 1,
        max: 31,
      },
      errorMessage: 'payment must be >= 1  and <= 31',
    },
  },
  id: {
    isEmpty: {
      errorMessage: 'id prop is invalid',
    },
  },
});
exports.createSchema = createSchema;
