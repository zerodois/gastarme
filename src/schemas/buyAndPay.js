const { checkSchema } = require('express-validator/check');

const schema = checkSchema({
  amount: {
    isNumeric: {
      options: {
        min: 0,
        max: 999,
      },
      errorMessage: 'amount must be >= 0.00',
    },
  },
});
exports.schema = schema;
