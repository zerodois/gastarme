const { checkSchema } = require('express-validator/check');

const createSchema = checkSchema({
  email: {
    isEmail: true,
  },
  password: {
    isEmpty: {
      negated: true,
    },
  },
  name: {
    isEmpty: {
      negated: true,
    },
  },
  role: {
    isInt: {
      options: {
        min: 0,
        max: 1,
      },
    },
  },
  id: {
    isEmpty: true,
  },
});
exports.createSchema = createSchema;

const loginSchema = checkSchema({
  email: {
    isEmail: true,
  },
  password: {
    isEmpty: {
      negated: true,
    },
  },
});
exports.loginSchema = loginSchema;
