const { checkSchema } = require('express-validator/check');

const updateSchema = checkSchema({
  limit: {
    isNumeric: {
      options: {
        min: 0,
      },
      errorMessage: 'Limit must be >= 0',
    },
  },
});
exports.updateSchema = updateSchema;
