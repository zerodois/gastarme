const bodyParser = require('body-parser');
const { parseToken } = require('./middlewares');

const factory = (container) => {
  const { app } = container;
  app.use(bodyParser.json());
  app.use(parseToken);

  app.use('/buy', container.buyRoute);
  app.use('/cards', container.cardsRoute);
  app.use('/pay', container.payRoute);
  app.use('/users', container.usersRoute);
  app.use('/wallet', container.walletRoute);
  app.use('/wallets', container.walletsRoute);

  app.get('/', (req, res) => res.send('Hello'));

  return app;
};

module.exports = factory;
