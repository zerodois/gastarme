const lilianaCards = require('../tests/fixtures/cards');
const { organize } = require('./cards');

describe('Card utils test', () => {
  describe('Buy tests', () => {
    it('Buy with 1 card', () => {
      const response = organize({
        today: 28,
        cards: lilianaCards,
        amount: 400,
      });
      expect(response).toEqual([lilianaCards[0]]);
    });
    it('Same payment day', () => {
      const cards = [...lilianaCards];
      cards[0].payment = cards[1].payment;
      const response = organize({
        cards,
        today: 28,
        amount: 400,
      });
      expect(response.length).toEqual(1);
      expect(response[0].id).toEqual('AAAA-BBBB');
    });
    it('Same payment day', () => {
      const cards = [...lilianaCards];
      cards[0].payment = cards[1].payment;
      const response = organize({
        cards,
        amount: 400,
      });
      expect(response.length).toEqual(1);
      expect(response[0].id).toEqual('AAAA-BBBB');
    });
    it('Default day', () => {
      const allCards = [...lilianaCards];
      allCards[0].payment = allCards[1].payment;
      const cards = [allCards[1], allCards[0]];
      const response = organize({
        cards,
        today: 28,
        amount: 400,
      });
      expect(response.length).toEqual(1);
      expect(response[0].id).toEqual('AAAA-BBBB');
    });
    it('Same order', () => {
      const response = organize({
        cards: [lilianaCards[0], { ...lilianaCards[0], id: 'AAAA-WWWW' }],
        today: 28,
        amount: 400,
      });
      expect(response.length).toEqual(1);
      expect(response[0].id).toEqual('AAAA-BBBB');
    });
    it('Buy with 2 card', () => {
      const response = organize({
        today: 28,
        cards: lilianaCards,
        amount: 2500,
      });
      expect(response).toEqual(lilianaCards);
    });
  });
});
