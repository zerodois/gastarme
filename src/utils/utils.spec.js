const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const utils = require('./index');

const SECRET = 'MTG';
process.env.JWT_SECRET = 'MTG';

describe('Utilities test', () => {
  it('Hash test', async () => {
    const text = 'Nicol Bolas';
    const encrypted = await utils.hash(text);
    const valid = await bcrypt.compare(text, encrypted);
    expect(valid).toEqual(true);
  });

  it('Verify Hash', async () => {
    const text = 'Nicol Bolas';
    const encrypted = await bcrypt.hash(text, 10);
    const valid = await utils.verify(text, encrypted);
    expect(valid).toEqual(true);
  });

  it('Sign in token', async () => {
    const user = { name: 'Nicol Bolas' };
    const token = utils.sign(user);
    const decoded = jwt.verify(token, SECRET);
    expect(decoded).toMatchObject(user);
  });
});
