const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const { get } = require('lodash');

class ResponseError extends Error {
  constructor(props) {
    super(props);
    const statusCode = get(props, 'statusCode', httpStatus.UNPROCESSABLE_ENTITY);
    const message = get(props, 'message', props.toString());
    this.statusCode = statusCode;
    this.message = message;
  }
}
exports.ResponseError = ResponseError;

/**
 * Promise wrapper
 * @param {Promise} promise
 * @returns {Promise}
 */
const to = promise => promise
  .then(res => [null, res])
  .catch(err => [err, null]);
exports.to = to;

/**
 * Hash function
 * @param {String} plain
 */
const hash = plain => bcrypt.hash(plain, get(process.env, 'SALT', 10));
exports.hash = hash;

/**
 * JWT Sign function
 * @param {object} payload
 */
const sign = payload => jwt.sign(payload, process.env.JWT_SECRET, {
  expiresIn: get(process.env, 'JWT_DURATION', 3600),
});
exports.sign = sign;

/**
 * Hash function
 * @param {String} plain
 * @param {String} encrypted
 * @returns {Promise}
 */
const verify = (plain, encrypted) => bcrypt.compare(plain, encrypted);
exports.verify = verify;
