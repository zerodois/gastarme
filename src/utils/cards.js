const moment = require('moment');

/**
 * Organize cards to buy or pay
 * @param {object} options Option object
 * @param {Cards[]} options.cards All User Cards
 * @param {number} options.amount Amount to pay or buy
 * @param {number} [options.today] Day of the month
 * @param {Boolean} [options.payment] True if is payment, False if is a purchase
 * @returns {Cards[]|null} cards to make payment/buy or null if impossible
 */
const organize = (options) => {
  const {
    cards,
    today = new Date().getDate(),
    payment = false,
  } = options;
  let { amount } = options;

  const getDate = (day) => {
    const format = 'YYYY-MM-DD';
    const date = moment(moment().format(format), format);
    if (day < today) {
      date.add(1, 'month');
    }
    date.date(day);
    return date;
  };

  const getAvaible = card => Math.max(card.limit - card.payable, 0);
  const getToDebit = (card) => {
    if (payment) {
      return card.payable;
    }
    return Math.max(card.limit - card.payable, 0);
  };

  const sortFn = (c1, c2) => {
    const card1 = getDate(c1.payment);
    const card2 = getDate(c2.payment);
    if (card1.isAfter(card2)) return -1;
    if (card2.isAfter(card1)) return 1;
    // Se vence mesmo dia, ordenar por limit disponivel
    const av1 = getAvaible(c1);
    const av2 = getAvaible(c2);
    if (av2 > av1) return -1;
    if (av1 > av2) return 1;
    return 0;
  };

  const resp = [];
  const sort = [...cards].sort(sortFn);
  sort.find((card) => {
    amount = Math.max(0, amount - getToDebit(card));
    resp.push(card);
    return !amount;
  });
  return resp;
};
exports.organize = organize;
