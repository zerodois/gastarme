const express = require('express');
const awilix = require('awilix');
const { get } = require('lodash');
const { sequelize, Sequelize } = require('./database');
const server = require('./server');
const { hash, verify, sign } = require('./utils');
const { walletFactory } = require('./middlewares');
require('dotenv').config();

const app = express();
const port = get(process.env, 'PORT', 8080);

const container = awilix.createContainer();
container.register('app', awilix.asValue(app));
container.register('router', awilix.asFunction(() => express.Router()));
container.register('server', awilix.asFunction(server));
container.register('walletMiddleware', awilix.asFunction(walletFactory));
container.register('sequelize', awilix.asValue(sequelize));
container.register('Sequelize', awilix.asValue(Sequelize));
container.register('hash', awilix.asValue(hash));
container.register('verify', awilix.asValue(verify));
container.register('today', awilix.asValue(null));
container.register('sign', awilix.asValue(sign));

const formatName = (name, descriptor) => {
  const splat = descriptor.path.split('/');
  const namespace = splat[splat.length - 2].slice(0, -1);
  const upperNamespace = namespace.charAt(0).toUpperCase() + namespace.substring(1);
  return name + upperNamespace;
};
const config = {
  formatName,
};

container.loadModules([
  'src/routes/*.js',
  'src/controllers/!(*.spec).js',
  'src/models/*.js',
], config);

container.resolve('server').listen(port, () => {
  console.log(`listening on http://localhost:${port}`);
});
