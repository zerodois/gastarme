## Bem vindo!
Esse é a api do Gastar.me, o cartão de crédito virtual que facilita a sua vida e da MILHARES de pessoas pelo mundo (só que não).

### Como executar
Caso queira executar a api do Gastar.me manualmente, siga o passo a passo descrito a seguir:

- Tome café, ele é importante e te dá energia para os próximos passos.

- Arranje um banco de dados *Postgres* (pode ser local, remoto ou até roubado, desde que funcione)
- Instale as dependências com `yarn install` ou `npm i`
- Crie um arquivo `.env` com suas variáveis de ambiente. As variáveis necessárias estão ilustradas em `.env.example`
- Rode o projeto com `yarn start` ou `npm run start`. A aplicação vai iniciar na porta configurada na variável de ambiente `PORT` ou caso ela não exista, na porta `8080`

**OOOOOOOOOOOOOU**

Dockerizei toda a parada, então se quiser, pode buildar as imagens necessárias pelo Dockerfile ou ainda mandar aquele `docker-compose up -d` maroto.

### Testes

Creio que você que está lendo já imagina isso, mas quem avisa amigo é, então o comando para rodar os famigerados testes são: `npm test` ou `yarn test`. Para os testes foi usado `jest`, e os dados de coverage são salvos na pasta `coverage`, acho que vale a pena dar uma olhada :)

No geral é isso. Qualquer crítica, dúvida ou sujestão, pode entrar em contato comigo ;)

felipelopesrita@gmail.com (codinome zerodois).
