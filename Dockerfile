FROM node:12.2.0

RUN adduser gastarme
ENV HOME=/home/gastarme
ENV NODE_ENV=production

COPY package.json yarn.lock docker-entrypoint.sh $HOME/app/
COPY src/ $HOME/app/src

WORKDIR $HOME/app

RUN yarn install --production && \
    chown -R gastarme:gastarme $HOME/*

USER gastarme

EXPOSE 3000
ENTRYPOINT [ "./docker-entrypoint.sh" ]

